package mushy.mushroomlearning.it.mushy.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListDataPump {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> chanterelles = new ArrayList<String>();
        chanterelles.add("Fricassee of Chanterelles");

        List<String> porcini = new ArrayList<String>();
        porcini.add("Funghi e porcini");

        List<String> tacks = new ArrayList<String>();
        tacks.add("Chiodini alla pizzaiola");

        List<String> prataioli = new ArrayList<String>();
        prataioli.add("Spaghetti ai funghi prataioli");

        expandableListDetail.put("Chanterelles", chanterelles);
        expandableListDetail.put("Porcini", porcini);
        expandableListDetail.put("Tacks", tacks);
        expandableListDetail.put("Prataioli", prataioli);

        return expandableListDetail;
    }
}