package mushy.mushroomlearning.it.mushy.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import mushy.mushroomlearning.it.mushy.R;

public class LeaderboardActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
