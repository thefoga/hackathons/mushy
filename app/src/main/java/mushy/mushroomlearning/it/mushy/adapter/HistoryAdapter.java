package mushy.mushroomlearning.it.mushy.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import mushy.mushroomlearning.it.mushy.fragment.FavoritePlacesFragment;
import mushy.mushroomlearning.it.mushy.fragment.PastLocationsFragment;
import mushy.mushroomlearning.it.mushy.fragment.PastTripsFragment;


/**
 * Pager for RealTimeTelemetry activity
 */
public class HistoryAdapter extends FragmentPagerAdapter {

    public HistoryAdapter(FragmentManager fm, Context context) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new PastLocationsFragment();
        } else if (position == 1) {
            return new PastTripsFragment();
        } else {
            return new FavoritePlacesFragment();
        }
    }

    @Override
    public int getCount() {
        // number of pages
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Past Locations";
            case 1:
                return "Past Trips";
            case 2:
                return "Favorite places";
        }
        return null;
    }
}
